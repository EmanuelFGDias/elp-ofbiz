package script;
import java.util.Map;


import org.ofbiz.base.util.Debug;
import org.ofbiz.entity.Delegator;
import org.ofbiz.entity.GenericEntityException;
import org.ofbiz.entity.GenericValue;
import org.ofbiz.service.DispatchContext;
import org.ofbiz.service.ServiceUtil;

public class MyServices {

 public static final String module = MyServices.class.getName();

 public static Map<String, Object> toUppercase(DispatchContext dctx, Map<String, ? extends Object> context) {
	 // Use the following to access entities or other services of ofbiz
		// Delegator delegator = dctx.getDelegator() ;
		// LocalDispatcher dispatcher = dctx.getDispatcher () ;
		// Get the msg input parameter
		String msg = (String)context.get("msg");
		String msgUpper=msg.toUpperCase();
		Map<String,Object> result = ServiceUtil.returnSuccess();
		result.put("msgUpper", msgUpper);
		return result;
	}
}
