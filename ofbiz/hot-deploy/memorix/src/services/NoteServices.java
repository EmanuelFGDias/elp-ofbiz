package org.ofbiz.memorixd.services;

import java.util.Map;
import java.time.LocalTime;
import java.time.LocalDate;

import org.ofbiz.base.util.Debug;
import org.ofbiz.base.util.UtilMisc;
import org.ofbiz.entity.Delegator;
import org.ofbiz.entity.GenericEntityException;
import org.ofbiz.entity.GenericValue;
import org.ofbiz.service.DispatchContext;
import org.ofbiz.service.ServiceUtil;

import org.ofbiz.base.util.UtilValidate;
import org.ofbiz.entity.util.EntityUtil;

import java.util.*;


public class NoteServices {
  

  public static Map<String, Object> registerTextNote(DispatchContext dctx, Map<String, Object> context) {
    Debug.logInfo("-->>NoteServices.registerTextNote() - ENTER","memorixd");

    // Get the msg input parameters:
    //String nType = (String) context.get("noteType");
    String nContent = (String) context.get("noteContent");
    Debug.logInfo("NoteServices.registerTextNote(): received data: nContent:\""+nContent+"\"","memorixd");
    
    //Validate fields:
    //
    if(null == nContent || nContent.isEmpty()) {
      Debug.logFatal("NoteServices.registerTextNote(): parameters can't be empty (noteContent)","memorixd");
      return ServiceUtil.returnError("parameters can't be empty");
    }
    /*
    switch(nType) {
      case "img":
      case "snd":
      case "vdo":
        Debug.logError("NoteServices.registerTextNote(): note type \""+nType+"\" not implemented","memorixd");
        return ServiceUtil.returnError("unsupported note type \""+nType+"\", please file a bug");
      case "txt":
        break;
      default:
        Debug.logFatal("NoteServices.createMxNote(): invalid note type \""+nType+"\"","memorixd");
        return ServiceUtil.returnError("note type \""+nType+"\" does not exist");
    }
    */
    Debug.logInfo("NoteServices.registerTextNote(): valid nContent:\""+nContent+"\"","memorixd");
    
    // Register Note
    //
    Delegator delegator = dctx.getDelegator();
    GenericValue note = delegator.makeValue("MemorixNote");//noteSeqID
    GenericValue hashtag = delegator.makeValue("MemorixHashtag");
    GenericValue noteHash = delegator.makeValue("MemorixNoteHashtag");
    GenericValue user = delegator.makeValue("MemorixUser");
    GenericValue userNote = delegator.makeValue("MemorixUserNote");
    //GenericValue txtNote = delegator.makeValue("NoteDescription");
    
    String outValue = "";
    try {
      String noteSeqID = outValue = delegator.getNextSeqId("MemorixNote");
      note.set("NoteID", noteSeqID);
      Debug.logInfo("********SEE THIS!!!!! -> "+noteSeqID,"memorixd");
      note.set("UserID", "000");      
      note.set("NoteDescription", nContent);
      note.set("Type", "Text");
      note.set("Location", "unknown");
      //note.set("Date", LocalDate.now());
      //note.set("Time", LocalTime.now());
      note.set("Date", "00");
      note.set("Time", "00");
      delegator.create(note);
      List<String> listWords = Arrays.asList(nContent.split("\\s+"));
      Debug.logInfo("%%%%%%%%%%%%%%BEFORE LIST!!! -> ","memorixd");
      for (int i = 0; i < listWords.size(); i++) {
    	  //System.out.println(listWords.get(i));
    	  Debug.logInfo("********IN LIST!!! -> "+listWords.get(i),"memorixd");
    	  Debug.logInfo("**1stword!!! -> "+listWords.get(i).substring(0, 1),"memorixd");
    	  Debug.logInfo("**OTHERSword!!! -> "+listWords.get(i).substring(1),"memorixd");
    	  
    	  //hashtag creation
    	  if (listWords.get(i).substring(0, 1).toLowerCase().equals("#")) {
    		  String hashtagSeqID = delegator.getNextSeqId("MemorixHashtag");    		  
    		  GenericValue lastDimensionValue=null;
    		  Debug.logInfo("********HASHTAG!!!!! -> "+hashtagSeqID,"memorixd");
    		  lastDimensionValue = EntityUtil.getFirst(delegator.findByAnd("MemorixHashtag",UtilMisc.toMap("HashtagDescription",listWords.get(i).substring(1).toLowerCase()),UtilMisc.toList("HashtagID")));    		  
    		  if (UtilValidate.isNotEmpty(lastDimensionValue)) {
    			  hashtagSeqID = lastDimensionValue.getString("HashtagID");
    		  } else {
        		  Debug.logInfo("********HASHTAG!!!!! -> "+hashtagSeqID,"memorixd");
        		  hashtag.set("HashtagID", hashtagSeqID);
        		  hashtag.set("HashtagDescription", listWords.get(i).substring(1).toLowerCase());
        		  delegator.create(hashtag);
        		  Debug.logInfo("********HASHTAGCREATED!","memorixd");
    		  }
    		  /*
    		  String hashtagSeqID = delegator.getNextSeqId("MemorixHashtag");
    		  Debug.logInfo("********HASHTAG!!!!! -> "+hashtagSeqID,"memorixd");
    		  hashtag.set("HashtagID", hashtagSeqID);
    		  hashtag.set("HashtagDescription", listWords.get(i).substring(1).toLowerCase());
    		  delegator.create(hashtag);
    		  Debug.logInfo("********HASHTAGCREATED!","memorixd");
    		  */
    		  noteHash.set("NoteID", noteSeqID);
    		  noteHash.set("HashtagID", hashtagSeqID);
    		  noteHash.set("NTimes", "1");
    		  delegator.create(noteHash);
    		  Debug.logInfo("********HASHTAGNOTECREATED!","memorixd");    		  
    	  }
    	  
    	  //notesusers creation
    	  if (listWords.get(i).substring(0, 1).toLowerCase().equals("@")) {
    		  String userSeqID = delegator.getNextSeqId("MemorixUser");
    		  GenericValue lastDimensionValue=null;
    		  Debug.logInfo("********USER!!!!! -> "+userSeqID,"memorixd");
    		  lastDimensionValue = EntityUtil.getFirst(delegator.findByAnd("MemorixUser",UtilMisc.toMap("Name",listWords.get(i).substring(1).toLowerCase()),UtilMisc.toList("UserID")));    		  
    		  if (UtilValidate.isNotEmpty(lastDimensionValue)) {
    			  userSeqID = lastDimensionValue.getString("UserID");
    		  } else {

    			  Debug.logInfo("********USER!!!!! -> "+userSeqID,"memorixd");//xxxHERE
    			  user.set("UserID", userSeqID);
    			  user.set("Name", listWords.get(i).substring(1).toLowerCase());
    			  user.set("Email", "TEST@TEST");
        		  delegator.create(user);
        		  Debug.logInfo("********USERCREATED!","memorixd");
    		  }
    		  /*
    		  Debug.logInfo("********NOTEUSER!!!!! -> "+hashtagSeqID,"memorixd");
    		  hashtag.set("HashtagID", hashtagSeqID);
    		  hashtag.set("HashtagDescription", listWords.get(i).substring(1).toLowerCase());
    		  delegator.create(hashtag);
    		  */
    		  userNote.set("NoteID", noteSeqID);
    		  userNote.set("UserID", userSeqID);
    		  delegator.create(userNote);
    		  Debug.logInfo("********NOTEUSERCREATED!","memorixd");    		  
    	  }
    	  
  	  }
      /*List<String> hastagsList = new ArrayList<String>();
      List<String> usersList = new ArrayList<String>();*/
      
      /*
      txtNote.set("noteID",noteSeqID);
      txtNote.set("noteContent", nContent);
      delegator.create(txtNote);
      */
      
    }
    catch (GenericEntityException gee){
      Debug.logFatal("NoteServices.registerTextNote(): GenericEntityException on delegator.create(note): "+ gee.getMessage(), "memorixd");
      return ServiceUtil.returnFailure();   
    }
    catch (Exception e){
      Debug.logFatal("NoteServices.registerTextNote(): Exception on delegator.create(note): "+e.getMessage(), "memorixd");
      return ServiceUtil.returnFailure();   
    }
    
    Map<String, Object> result = ServiceUtil.returnSuccess();
    result.put("noteID", outValue);
    Debug.logInfo("********SEE THIS!!!!! -> " + outValue,"memorixd");
    return result;
  }
  
  /**
   * For the sole purpose of having something quick to test
   * */
  public String upper4tests(String str) {
	  return str.toUpperCase();
  }
  
  public static Map<String, Object> viewListNote(DispatchContext dctx, Map<String, Object> context) {
	    Debug.logInfo("NoteServices.viewListNote() - ENTER","memorixd");

	    // Get the msg input parameters:
	    //
	    String hashtag = (String) context.get("hashtag");
	    Debug.logInfo("NoteServices.viewListNote(): received data: nID:\""+hashtag+"\"","memorixd");
	    
	    // validate fields:
	    //
	    if(null == hashtag || hashtag.isEmpty()) {
	      Debug.logFatal("NoteServices.viewListNote(): parameter can't be empty (nID)","memorixd");
	      return ServiceUtil.returnError("parameters can't be empty");
	    }
	    /*
	    Integer iNID;
	    	    try {
	    	iNID = Integer.parseInt(hashtag);
		} catch (NumberFormatException nfe) {
			Debug.logFatal("NoteServices.viewListNote(): parameter nID is not integer (\""+nID+"\")","memorixd");
		    return ServiceUtil.returnError("invalid parameters");
		}
		*/
	    Debug.logInfo("NoteServices.getMxNoteByID(): valid data: nID:"+hashtag+"","memorixd");
	    
	    // insert
	    //
	    Delegator delegator = dctx.getDelegator();
	    //GenericValue note = delegator.makeValue("MemorixNote");//noteSeqID
	    //GenericValue txtNote = delegator.makeValue("TextNote");
	    
	    //xxx
	    Map<String, Object> result = ServiceUtil.returnSuccess();
	    //Map<String,? extends Object> naturalKeyFields=UtilGenerics.cast(context.get("hashtag"));
	    GenericValue lastDimensionValue=null;
	    try {
	    	Debug.logInfo("********START!!!!! -> ","memorixd");
	      List<GenericValue> listHashtagMatch =delegator.findByAnd("MemorixHashtag",UtilMisc.toMap("HashtagDescription",hashtag.toLowerCase()),UtilMisc.toList("HashtagID"));
	      for (GenericValue hashtagMatch : listHashtagMatch) {
	    	  Debug.logInfo("********HASHTAGLIST ENTROU!!!!! -> "+ hashtagMatch.getString("HashtagID"),"memorixd");
	          /*GenericValue newGeo=delegator.findOne("Geo",true,"geoId",geoAssoc.getString("geoId"));
	          geoIdByTypeMapTemp.put(newGeo.getString("geoTypeId"),newGeo.getString("geoId"));*/
	    	  GenericValue noteLink=EntityUtil.getFirst(delegator.findByAnd("MemorixNoteHashtag",UtilMisc.toMap("HashtagID", hashtagMatch.getString("HashtagID")),UtilMisc.toList("NoteID")));
	    	  if (UtilValidate.isNotEmpty(noteLink)) {
	  	    	Debug.logInfo("********ENTER HASHTAG--SEE THIS!!!!! -> " ,"memorixd");
	  	    	GenericValue noteMatch =EntityUtil.getFirst(delegator.findByAnd("MemorixNote",UtilMisc.toMap("NoteID", noteLink.getString("NoteID")),UtilMisc.toList("NoteID")));
	  	    	result.put("sendNotes", result.get("sendNotes")+"$"+noteMatch.getString("NoteID")+"&"+noteMatch.getString("NoteDescription"));
	  	      }
	        }
	      //delegator.findByAnd("MemorixNote", UtilMisc.toMap("NoteDescription", hashtag), orderBy, false);
	    }
	   catch (  GenericEntityException gee) {
	      return ServiceUtil.returnError(gee.getMessage());
	    }
	    /*
	    Debug.logInfo("********SEE THIS!!!!! -> "+ lastDimensionValue.getString("NoteID"),"memorixd");
	    
	    if (UtilValidate.isNotEmpty(lastDimensionValue)) {
	    	Debug.logInfo("********ENTROU! SEE THIS!!!!! -> " ,"memorixd");
	        
	    	result.put("sendNotes",lastDimensionValue.getString("NoteID"));
	    }
	    */
	    Debug.logInfo("********END!!!!!! -> " +  result.get("sendNotes"),"memorixd");
	    return result;
	    //xxx
	    
	    
	    /*
	    //xxx
	    Map<String, Object> result = ServiceUtil.returnSuccess();
	    //Map<String,? extends Object> naturalKeyFields=UtilGenerics.cast(context.get("hashtag"));
	    GenericValue lastDimensionValue=null;
	    try {
	      lastDimensionValue=EntityUtil.getFirst(delegator.findByAnd("MemorixNote",UtilMisc.toMap("NoteDescription",hashtag),UtilMisc.toList("-Time")));
	      //delegator.findByAnd("MemorixNote", UtilMisc.toMap("NoteDescription", hashtag), orderBy, false);
	    }
	   catch (  GenericEntityException gee) {
	      return ServiceUtil.returnError(gee.getMessage());
	    }
	    
	    Debug.logInfo("********SEE THIS!!!!! -> "+ lastDimensionValue.getString("NoteID"),"memorixd");
	    
	    if (UtilValidate.isNotEmpty(lastDimensionValue)) {
	    	Debug.logInfo("********ENTROU! SEE THIS!!!!! -> " ,"memorixd");
	        
	    	result.put("sendNotes",lastDimensionValue.getString("NoteID"));
	    }
	    return result;
	    //xxx
	    
        String outValue = "";
	    try {
	      String noteSeqID = outValue = delegator.getNextSeqId("Note");
	      note.set("noteID", noteSeqID);
	      note.set("noteType", nID);
	      delegator.create(note);
	      txtNote.set("noteID",noteSeqID);
	      txtNote.set("noteContent", nContent);
	      delegator.create(txtNote);
	    }
	    catch (GenericEntityException gee){
	      Debug.logFatal("NoteServices.createMxNote(): GenericEntityException on delegator.create(note): "+gee.getMessage(),"memorixd");
	      return ServiceUtil.returnFailure();   
	    }
	    catch (Exception e){
	      Debug.logFatal("NoteServices.createMxNote(): Exception on delegator.create(note): "+e.getMessage(),"memorixd");
	      return ServiceUtil.returnFailure();   
	    }
	    
	    Map<String, Object> result = ServiceUtil.returnSuccess();
	    result.put("noteID", outValue);
	    return result;
	    */
	  }
}
